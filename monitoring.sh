#!/bin/bash
set -e
################################################################
if [ ! -d "/u01" ] 
then
    mkdir /u01  
fi
cp *.service /u01
cp *.yml /u01
cd /u01
#################################################################
#configure nomad config to publish metric to prometheus
# tee -a /etc/nomad.d/nomad.hcl > /dev/null <<EOT
# telemetry {
#  collection_interval = "5s",
#  publish_allocation_metrics = true,
#  publish_node_metrics = true,
#  prometheus_metrics = true
# }
# EOT
################################################################
#Download and Unpack Prometheus
wget https://github.com/prometheus/prometheus/releases/download/v2.19.1/prometheus-2.19.1.linux-amd64.tar.gz
tar xfz prometheus-2.19.1.linux-amd64.tar.gz
mv prometheus-2.19.1.linux-amd64 prometheus
rm -rf prometheus-2.19.1.linux-amd64.tar.gz
#################################################################
#Update prometheus configuration file for node exporter
tee -a prometheus/prometheus.yml > /dev/null <<EOT
  # Localhost Node Exporter ( You can add all servers here)
  - job_name: 'node_exporter'
    scrape_interval: 5s
    static_configs:
      - targets: ['localhost:9100']
      
  # Nomad Metric configuration
  - job_name: nomad
    metrics_path: "/v1/metrics"
    params:
      format: ['prometheus']
    scheme: http
    static_configs:
      - targets:
         - localhost:4646
EOT
#################################################################
#Download and Unpack the Node Exporter
wget https://github.com/prometheus/node_exporter/releases/download/v1.0.1/node_exporter-1.0.1.linux-amd64.tar.gz
tar xvf node_exporter-1.0.1.linux-amd64.tar.gz
mv node_exporter-1.0.1.linux-amd64 node_exporter
rm -rf node_exporter-1.0.1.linux-amd64.tar.gz
#################################################################
#Download and Unpack Grafana
wget https://dl.grafana.com/oss/release/grafana-7.0.4.linux-amd64.tar.gz
tar -zxvf grafana-7.0.4.linux-amd64.tar.gz
mv grafana-7.0.4 grafana
rm -rf grafana-7.0.4.linux-amd64.tar.gz
#################################################################
#Download and Unpack Loki
curl -O -L "https://github.com/grafana/loki/releases/download/v1.6.1/loki-linux-amd64.zip"
unzip "loki-linux-amd64.zip"
chmod a+x "loki-linux-amd64"
mv loki-linux-amd64 loki
#################################################################
#Download and Unpack Promtail 
curl -fSL -o promtail.gz "https://github.com/grafana/loki/releases/download/v1.6.1/promtail-linux-amd64.zip"
gunzip promtail.gz
chmod a+x promtail
url=`nomad status mpay| grep running| awk '{print $1}' | awk 'NR==2 {print $1}'`
sed -ie "s/abcd/$url*/g" /u01/config-promtail.yml
#################################################################
#Create service for monitoring tools
cp /u01/*.service /etc/systemd/system/
systemctl enable node_exporter prometheus grafana loki promtail
systemctl start node_exporter prometheus grafana loki promtail 
#################################################################

